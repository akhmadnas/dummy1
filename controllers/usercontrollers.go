package controllers

import (
	"encoding/json"
	"net/http"

	"dummy1/database"
	"dummy1/entity"

	"github.com/gorilla/mux"
)

// GetAllPerson get all person data
func GetAllUser(w http.ResponseWriter, r *http.Request) {
	var user []entity.User
	database.Connector.Find(&user)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(user)
}

// GetPersonByID returns person with specific ID
func GetUserByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]

	var user entity.User
	database.Connector.First(&user, key)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)
}
