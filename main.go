package main

import (
	"log"
	"net/http"

	"dummy1/controllers"
	"dummy1/database"
	"dummy1/entity"

	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql" //Required for MySQL dialect
)

func main() {
	initDB()
	log.Println("Starting the HTTP server on port 8080")

	router := mux.NewRouter().StrictSlash(true)
	initaliseHandlers(router)
	log.Fatal(http.ListenAndServe(":8080", router))
}

func initaliseHandlers(router *mux.Router) {
	router.HandleFunc("/get", controllers.GetAllUser).Methods("GET")
	router.HandleFunc("/get/{id}", controllers.GetUserByID).Methods("GET")
}

func initDB() {
	config :=
		database.Config{
			ServerName: "testlab:3306",
			User:       "root",
			Password:   "anan123",
			DB:         "tes",
		}

	connectionString := database.GetConnectionString1(config)
	err := database.Connect(connectionString)
	if err != nil {
		panic(err.Error())
	}
	database.Migrate(&entity.User{})
}
