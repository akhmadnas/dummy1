package entity

//Person object for REST(CRUD)
type User struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Status string `json:"status"`
}
