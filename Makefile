PROJECT_ID=go-dummy
LOCAL_TAG=go-dummy:101
REMOTE_TAG=akhmadnas/$(PROJECT_ID):go-dummy-101
CONTAINER_NAME=my_golang

build:
	docker build -t $(LOCAL_TAG) .

push:
	docker tag $(LOCAL_TAG) $(REMOTE_TAG)
	docker push $(REMOTE_TAG)

deploy: 
	./containercond.sh
