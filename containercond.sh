#!/bin/bash
CONTAINER_NAME='my_golang'
LOCAL_TAG='go-dummy:101'

if [ $( docker ps -a -f name=$CONTAINER_NAME | wc -l ) -eq 2 ]; then
    echo "Container exists" &&
    docker container stop $CONTAINER_NAME &&
	docker container rm $CONTAINER_NAME &&
    docker run -d --name=$CONTAINER_NAME --restart=unless-stopped -p 8080:8080 -d --network=bc_network $LOCAL_TAG
else
    echo "Container does not exist" &&
  	docker run -d --name=$CONTAINER_NAME --restart=unless-stopped -p 8080:8080 -d --network=bc_network $LOCAL_TAG
fi
